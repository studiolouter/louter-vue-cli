import { app, BrowserWindow } from "electron";
// import { pathToFileURL } from "url";
import * as path from 'path'
// import * as Sentry from "@sentry/electron";
// const SentryClient = Sentry.SentryClient;
// const Raven = require("raven");

// transporter.verify(function(error, success) {
//   if (error) {
//     console.error(error);
//     SentryClient.captureException(new Error(error));
//   } else {
//     console.log("Server is ready to take our messages");
//   }
// });

// SentryClient.create({
//   dsn:
//     "https://9d400a2915bf4991a5468473d98c3e9c:a622185a5ff542c0bb0caf03cd77add5@sentry.io/356898",
// });



/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== "development") {
  global.__static = require("path")
    .join(__dirname, "/static")
    .replace(/\\/g, "\\\\");
}

let mainWindow;
const winURL = getEnvMode()
  

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    useContentSize: true,
    height: 1920,
    width: 1080,
    fullscreen: true,
    kiosk: true,
  });

  mainWindow.loadURL(winURL);

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

app.on("ready", () => {
  createWindow();
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    // TURN ON WHEN PRODUCTION
    if (process.env.NODE_ENV === "production") {
        app.quit();
    } else {
      app.quit();
    }
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */

function getEnvMode () {
  console.log(path.join('file://', __dirname, '../', '/dist', '/electron', '/index.html'))
  return 'file://' + path.join(__dirname, '../../', '/dist', '/electron', '/index.html')
  // if (process.env.MODE === "development") return `http://localhost:9080`
  // else if (process.env.MODE === "production") return `file://${__dirname}/index.html`
  // else return path.join(__dirname, '/dist', '/electron', '/index.html')
}